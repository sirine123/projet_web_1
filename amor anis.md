semaine1: 

brainstorming : recherche d'idée

semaine2:

prototype/maquette

semaine3:

établir le code HTML de la page d'accueil .

codage html : <!DOCTYPE html>
<html lang="fr">
<head>
	<title>
		Architecture and Interior Designs
	</title>
	<link rel="stylesheet" href="interior_style.css">
	<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.4.2/css/all.css' integrity='sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns' crossorigin='anonymous'>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>




<body>
	<div class="aaa" style="margin-top : 70px;margin-bottom: 20px; position: fixed; padding: 20%;">

	</div>
	<div class="list" align="right">
		<div class="middle-btn" style="margin-top : 20px;margin-bottom: 20px; position: fixed;">
						
						<a href="#slides2" >
							Nos Métiers
						</a>
						<a href="#slides3" target="_self">
							Ce que nous faisons pour votre bâtiment
						</a>
						<a href="#slides4" target="_self">
							Planification architecturale
						</a>
						<a href="#slides5" target="_self">
							Dernières nouvelles du blog
						</a>
					</div>
	</div>
	<div class="slideshow-container">
	

		<div class="mySlides fade">
			<div id="slides">
			</div>
		  <img src="Images/unnamed.jpg" style="width:100%">
		  <div class="text2">Designey architect d'interieur</div>
		</div>
		
		<div class="mySlides fade">
		  <div class="numbertext">2 / 3</div>
		  <img src="Images/claustra-pour-separer-chambre-et-coin-bureau-du-salon-petit-espace_5796455.jpg" style="width:100%;">
		  <div class="text2">Créateurs d'interieur</div>
		</div>
		
		<div class="mySlides fade">
		  <div class="numbertext">3 / 3</div>
		  <img src="Images/hero.jpg" style="width:100%">
		  <div class="text2" >
			nous partageons le pouvoir de l'architecture pour créer un changement positif</div>
		</div>
		
		</div>
		<br>
		
		<div style="text-align:center">
		  <span class="dot"></span> 
		  <span class="dot"></span> 
		  <span class="dot"></span> 
		</div>
		
		<script>
		var slideIndex = 0;
		showSlides();
		
		function showSlides() {
		  var i;
		  var slides = document.getElementsByClassName("mySlides");
		  var dots = document.getElementsByClassName("dot");
		  for (i = 0; i < slides.length; i++) {
			slides[i].style.display = "none";  
		  }
		  slideIndex++;
		  if (slideIndex > slides.length) {slideIndex = 1}    
		  for (i = 0; i < dots.length; i++) {
			dots[i].className = dots[i].className.replace(" active", "");
		  }
		  slides[slideIndex-1].style.display = "block";  
		  dots[slideIndex-1].className += " active";
		  setTimeout(showSlides, 3000); // Change image every 2 seconds
		}
		</script>
		
semaine4:

Commencer le code CSS .

CODE CSS:
/* NORMALIZE et al begins */
html{line-height:1.15;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}body{margin:0}article,aside,footer,header,nav,section{display:block}h1{font-size:2em;margin:.67em 0}figcaption,figure,main{display:block}figure{margin:1em 40px}hr{box-sizing:content-box;height:0;overflow:visible}pre{font-family:monospace,monospace;font-size:1em}a{background-color:transparent;-webkit-text-decoration-skip:objects}abbr[title]{border-bottom:none;text-decoration:underline;text-decoration:underline dotted}b,strong{font-weight:inherit}b,strong{font-weight:bolder}code,kbd,samp{font-family:monospace,monospace;font-size:1em}dfn{font-style:italic}mark{background-color:#ff0;color:#000}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sub{bottom:-.25em}sup{top:-.5em}audio,video{display:inline-block}audio:not([controls]){display:none;height:0}img{border-style:none}svg:not(:root){overflow:hidden}button,input,optgroup,select,textarea{font-family:sans-serif;font-size:100%;line-height:1.15;margin:0}button,input{overflow:visible}button,select{text-transform:none}button,html [type=button],[type=reset],[type=submit]{-webkit-appearance:button}button::-moz-focus-inner,[type=button]::-moz-focus-inner,[type=reset]::-moz-focus-inner,[type=submit]::-moz-focus-inner{border-style:none;padding:0}button:-moz-focusring,[type=button]:-moz-focusring,[type=reset]:-moz-focusring,[type=submit]:-moz-focusring{outline:1px dotted ButtonText}fieldset{padding:.35em .75em .625em}legend{box-sizing:border-box;color:inherit;display:table;max-width:100%;padding:0;white-space:normal}progress{display:inline-block;vertical-align:baseline}textarea{overflow:auto}[type=checkbox],[type=radio]{box-sizing:border-box;padding:0}[type=number]::-webkit-inner-spin-button,[type=number]::-webkit-outer-spin-button{height:auto}[type=search]{-webkit-appearance:textfield;outline-offset:-2px}[type=search]::-webkit-search-cancel-button,[type=search]::-webkit-search-decoration{-webkit-appearance:none}::-webkit-file-upload-button{-webkit-appearance:button;font:inherit}details,menu{display:block}summary{display:list-item}canvas{display:inline-block}template{display:none}[hidden]{display:none}

.clearfix:after {visibility: hidden; display: block; font-size: 0; content: ''; clear: both; height: 0; }

html { -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box;}

*, *:before, *:after {box-sizing: inherit}

.sr-only { position: absolute; width: 1px; height: 1px; margin: -1px; border: 0; padding: 0; white-space: nowrap; clip-path: inset(100%); clip: rect(0 0 0 0); overflow: hidden;}

html {
    font-size: 62.5%; 
}

body {
    background: #F4F3F0;
    font-family: 'Open Sans', sans-serif;
    font-size: 1.8rem;
    color: #000;
     overflow-x: hidden;
}

section {
    margin: 75px 0;
}


.wrapper {
    max-width: 1200px;
    width: 85%;
    margin: 0 auto;
}

h1 {
    font-size: 4.2rem;
    text-align: center;
    line-height: 65px;
    width: 750px;
    padding: 75px 0 100px 0;
}

h1,
h2,
h3,
button,
p.clientName,
.footerNav p {
    font-weight: 600;
}

h1,
h3,
.location p {
    margin: 0;
}

h2 {
    font-size: 3.2rem;
}

h3 {
    display: inline-block;
    background-color: #fff;
    padding: 10px 20px;
    margin: 10px;
}


h3,
button {
    font-size: 1.6rem;
}
p {
    line-height: 30px;
}

span {
    display: inline-block;
    border-bottom: 2px solid #B18F39;
    padding-bottom: 10px;
}

button {
    color: #FFF;
    background: #B18F39;
    border: none;
    padding: 15px 25px;
}

nav,
p {
    font-weight: 400;
}

img {
    width: 100%;
    height: 100%;
    object-fit: cover;
}

a {
    color: inherit;
    text-decoration: none;
}

ul {
    list-style: none;
}
.headerLinks a:hover {
    color:#B18F39;
}


semaine5:

Finir une partie du code CSS et se réunir avec l'équipe pour organiser les sections du travail et établir un code complet .

CODE CSS:
.arrow {
    width: 20%;
    position: absolute;
    right: 0;
    bottom: 25px;
}

.arrow img {
    object-fit: contain;
}

.footerImg {
    height: 420px;
}

.footer-list p {
    padding-left: 40px;
    margin: 30px 0 10px 0;
}

.footer-list a {
    display: inline-block;
    text-decoration: underline;
    padding: 10px 0;
}


.footerContent {
    margin-top: 75px;
}

.footerInfo {
    width: 50%;
}

.footerNav {
    width: 50%;
}

@media (max-width: 975px) {
    
    h1 {
        font-size: 4rem;
        width: 75%;
        padding-top: 25px;
    }

    h2 {
        font-size: 3rem;
    }

    .wrapper {
        width: 95%;
    }
     
    .contentEnd p,
    .practiceText p,
    .spotlightContainer p,
    .contactContainer p,
    .location p,
    .contactInfo p,
    .footerNav a {
        font-size: 1.6rem;
    }
	.practiceText,
    .practice-assets {
        height: 500px;
    }

    .practiceText {
        padding: 10px;
    }

    .practiceText h2 {
        margin-top: 0;
    }

    .practiceText p {
        margin-bottom: 10px;
    }

    .practiceContent  button,
    .contactContainer button {
        font-size: 1.4rem;
        padding: 10px 20px;
        width: 100%;
    }
    .contactContainer button {
        width: 75%;
    }

    .clientText p {
        font-size: 2.8rem;
    }

    p.clientName {
        font-size: 2.2rem;
    }

    p.clientTitle,
    .footerNav p {
        font-size: inherit;
    }


    .spotlight1,
    .spotlight2 {
        height: 400px;
    }

    .contactForm {
        width: 75%;
        margin-top: 50px;
    }
}


@media (max-width: 760px) {
    .headerLinks {
        width: 90%;
    }
    
    .navContent {
        justify-content: center;
    }

    .practiceText,
    .practice-assets {
        width: 100%;
    }

    .practiceText {
        height: auto;
    }

    .practice-assets {
        height: 700px;
        margin-top: 50px;
    }

    .spotlight1,
    .spotlight2 {
        height: 300px;
    }

    .contactForm {
        width: 75%;
        margin-top: 50px;
    }

    .clientText {
        width: 100%;
    }

    .footerInfo,
    .footerNav {
        width: 100%;
    }

    .footerInfo {
        border-bottom: 1px solid #9C9B98;
    }

    .footerNav {
        flex-direction: column;
        width: 100%;
    }

    .footer-list {
        margin-top: 20px;
    }

    .footer-list p,
    .footer-list ul {
        padding: 0;
    }

    .footer-list p {
        margin: 0;
    }

    .footer-list ul {
        display: flex;
        width: 100%;
    }

    .footer-list li {
        padding-right: 50px;
    }
}

