# projet_web_1

Notre projet est un site web nommé "designey" qui est un studio d'architecture . Il vise à montrer aux clients nos services offerts et des exemples de nos travails 
ainsi que nos coordonnées pour nous contacter ou nous visiter sur place pour avoir plus d'informations .
En fait, la page d'accueil contient le nom , le logo et des images qui représentent nos designs les plus connus de notre studio. la page suivante "our practice"
contient une petite description sur les actions proposées , puis une page "projects" contenant quelques exemples des projets achevés par le studio . De plus ,
"tell us about your ideas" est un formulaire de contact où le client peut envoyer des demandes ou des questions à l'équipe pour de meilleurs renseignements .
On présente également une page de feedback "words from our clients" qui montre l'avis des anciens clients. A la fin , on dispose de nos contacts .