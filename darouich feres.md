semaine1: 

brainstorming : recherche d'idée

semaine2:

prototype/maquette

semaine3:

établir le code HTML de la page "Projects " et la page "Feedback".

CODE HTML:
            <!-- spotlight section-->
            <section class="spotlightContainer">
                <h2><span>Projects</span></h2>
                <p>We present to you our latest designs.</p>
                <div class="spotlight-assets container" id="projects">
                    <div class="spotlight1">
                        <img src="./static/image3.jpg" alt="Kitchen3">
                        <h3>Elouise Avenue</h3>
                    </div>
                    <div class="spotlight1">
                        <img src="./static/image4.jpg" alt="Modern house">
                        <h3>Grant Residence</h3>
                    </div>
                    <div class="spotlight2">
                        <img src="./static/image5.jpg" alt="Nice bright room">
                        <h3>Basil Lab</h3>
                    </div>
                    <div class="spotlight2">
                        <img src="./static/image6.jpg" alt="Nice bright room2">
                        <h3>Jo Residence</h3>
                    </div>
                    <div class="spotlight2">
                        <img src="./static/image7.jpg" alt="Nice bright room3">
                        <h3>Rentea</h3>
                    </div>
                </div>
            </section>
            <!-- spotlight section ends-->
			
	 <div class="wrapper">
            <!-- client section -->
            <section class="clientContainer container" id="boom">
                <h2><span>Words from our clients</span></h2>
                <div class="clientText">
                    <p>designey were an incredible team of passionate creatives, who truly transformed our space into a beautiful sanctuary.</p>
                    <div class="clientInfo container">
                        <div class="arrowProfile container">
                            <div class="clientImgProfile container">
                                <div class="clientImg">
                                    <img src="./static/image10.jpg" class="clientImg" alt="client photo">
                                </div>
                                <div class="clientProfile container">
                                    <p class="clientName">Ameni Amouna</p>
                                    <p class="clientTitle">Founder, RENTEA</p>
                                </div>
                            </div>
                            <div class="arrow">
                                <img src="./static/arrow.svg" alt="arrow pic">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- end op {
    line-height: 30px;
}

span {
    display: inline-block;
    border-bottom: 2px solid #B18F39;
    padding-bottom: 10px;
}

button {
    color: #FFF;
    background: #B18F39;
    border: none;
    padding: 15px 25px;
}

nav,
p {
    font-weight: 400;
}

img {
    width: 100%;
    height: 100%;
    object-fit: cover;
}

a {
    color: inherit;
    text-decoration: none;
}

ul {
    list-style: none;
}
.headerLinks a:hover {
    color:#B18F39;
}

.logo {
    font-size: 2.8rem;
    color:#B18F39;
}

.logo,
.headerLinks, 
button, 
h3 {
    text-transform: uppercase;
}
 
  .numbertext {
    color: #f2f2f2;
    font-size: 12px;
    padding: 8px 12px;
    position: absolute;
    top: 0;
  }
  
 
  .dot {
    height: 15px;
    width: 15px;
    margin: 0 2px;
    background-color: #bbb;
    border-radius: 50%;
    display: inline-block;
    transition: background-color 0.6s ease;
  }
  
  .active {
    background-color: #717171;
  }
  
  .fade {
    -webkit-animation-name: fade;
    -webkit-animation-duration: 1.5s;
    animation-name: fade;
    animation-duration: 1.5s;
  }
  
ul.headerLinks {
    padding: 0;
}

.headerLinks li {
    padding: 20px 0;
}

.practiceText p
 {
    margin: 30px 0;
}
.spotlightContainer p,
.spotlightContainer h2{
    margin-left:  10px;
}
.spotlightContainer p,
.contactContainer p {
    width: 480px;
}

p.clientTitle,
.footerNavList p {
    font-size: 2rem;
}


.clientText p {
    font-size: 3rem;
    line-height: 45px;
    width: 100%;
    margin: 0 0 45px 0;
}


p.clientName {
    font-size: 2.4rem;
}

p.clientName,
p.clientTitle {
    margin: 0;
}


.contentEnd {
    border-top: 1px solid #9C9B98;
    color: #9C9B98;
    margin-top: 30px;
    text-align: center;
}



.container {
    display: flex;
}

.navContent,
.practiceContent,
.spotlight-assets,
.contactForm,
.footerContent {
    flex-wrap: wrap;
}

.headerContent,
.nameForm,
.emailForm,
.message,
.clientContainer,
.clientInfo,
.clientProfile,
footer.container,
.footer-list {
    flex-direction: column;
}f client section-->		
            
  semaine 4:
  
  établir une partie du code CSS.
  
  CODE CSS:
  p {
    line-height: 30px;
}

span {
    display: inline-block;
    border-bottom: 2px solid #B18F39;
    padding-bottom: 10px;
}

button {
    color: #FFF;
    background: #B18F39;
    border: none;
    padding: 15px 25px;
}

nav,
p {
    font-weight: 400;
}

img {
    width: 100%;
    height: 100%;
    object-fit: cover;
}

a {
    color: inherit;
    text-decoration: none;
}

ul {
    list-style: none;
}
.headerLinks a:hover {
    color:#B18F39;
}

.logo {
    font-size: 2.8rem;
    color:#B18F39;
}

.logo,
.headerLinks, 
button, 
h3 {
    text-transform: uppercase;
}
 
  .numbertext {
    color: #f2f2f2;
    font-size: 12px;
    padding: 8px 12px;
    position: absolute;
    top: 0;
  }
  
 
  .dot {
    height: 15px;
    width: 15px;
    margin: 0 2px;
    background-color: #bbb;
    border-radius: 50%;
    display: inline-block;
    transition: background-color 0.6s ease;
  }
  
  .active {
    background-color: #717171;
  }
  
  .fade {
    -webkit-animation-name: fade;
    -webkit-animation-duration: 1.5s;
    animation-name: fade;
    animation-duration: 1.5s;
  }
  
ul.headerLinks {
    padding: 0;
}

.headerLinks li {
    padding: 20px 0;
}

.practiceText p
 {
    margin: 30px 0;
}
.spotlightContainer p,
.spotlightContainer h2{
    margin-left:  10px;
}
.spotlightContainer p,
.contactContainer p {
    width: 480px;
}

p.clientTitle,
.footerNavList p {
    font-size: 2rem;
}


.clientText p {
    font-size: 3rem;
    line-height: 45px;
    width: 100%;
    margin: 0 0 45px 0;
}


p.clientName {
    font-size: 2.4rem;
}

p.clientName,
p.clientTitle {
    margin: 0;
}


.contentEnd {
    border-top: 1px solid #9C9B98;
    color: #9C9B98;
    margin-top: 30px;
    text-align: center;
}



.container {
    display: flex;
}

.navContent,
.practiceContent,
.spotlight-assets,
.contactForm,
.footerContent {
    flex-wrap: wrap;
}

.headerContent,
.nameForm,
.emailForm,
.message,
.clientContainer,
.clientInfo,
.clientProfile,
footer.container,
.footer-list {
    flex-direction: column;
}
.navContent,
.headerLinks,
.practiceContent,
.contactForm,
.clientInfo,
footer.container,
.footerContent,
.footerNav {
    justify-content: space-between;
}

.headerContent {
    align-items: center;
}

.navContent {
    position: relative;
}

.headerLinks {
    width: 480px;
}

header {
    position: relative;
}

.badge {
    position: relative;
    top: -70px;
    left: 50%;
    margin-left: -70px;
}

.header-assets {
    width: 100%;
    position: relative;
}


semaine 5:
Finir une partie du code CSS et se réunir avec l'équipe pour organiser le code complet .

Code CSS:
 .practiceText,
    .practice-assets {
        height: 500px;
    }

    .practiceText {
        padding: 10px;
    }

    .practiceText h2 {
        margin-top: 0;
    }

    .practiceText p {
        margin-bottom: 10px;
    }

    .practiceContent  button,
    .contactContainer button {
        font-size: 1.4rem;
        padding: 10px 20px;
        width: 100%;
    }
    .contactContainer button {
        width: 75%;
    }

    .clientText p {
        font-size: 2.8rem;
    }

    p.clientName {
        font-size: 2.2rem;
    }

    p.clientTitle,
    .footerNav p {
        font-size: inherit;
    }


    .spotlight1,
    .spotlight2 {
        height: 400px;
    }

    .contactForm {
        width: 75%;
        margin-top: 50px;
    }
}


@media (max-width: 760px) {
    .headerLinks {
        width: 90%;
    }
    
    .navContent {
        justify-content: center;
    }

    .practiceText,
    .practice-assets {
        width: 100%;
    }

    .practiceText {
        height: auto;
    }

    .practice-assets {
        height: 700px;
        margin-top: 50px;
    }

    .spotlight1,
    .spotlight2 {
        height: 300px;
    }

    .contactForm {
        width: 75%;
        margin-top: 50px;
    }

    .clientText {
        width: 100%;
    }

    .footerInfo,
    .footerNav {
        width: 100%;
    }

    .footerInfo {
        border-bottom: 1px solid #9C9B98;
    }

    .footerNav {
        flex-direction: column;
        width: 100%;
    }

    .footer-list {
        margin-top: 20px;
    }

    .footer-list p,
    .footer-list ul {
        padding: 0;
    }

    .footer-list p {
        margin: 0;
    }

    .footer-list ul {
        display: flex;
        width: 100%;
    }

    .footer-list li {
        padding-right: 50px;
    }
}